from django.db import models


class Puesto_laboral(models.Model):
    id_puesto = models.IntegerField(primary_key=True)
    puesto = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = "Puestos Laborales"

    def __str__(self):
        return self.puesto

class Medida(models.Model):
    id_medida = models.IntegerField(primary_key=True)
    cantidad = models.DecimalField(max_digits=12, decimal_places=2)
    medida = models.IntegerField(default=0)

    class Meta:
        verbose_name_plural = "Medidas"

    def __str__(self):
        return self.id_medida

class Nit(models.Model):
    id_nit = models.IntegerField(primary_key=True)
    tipo = models.CharField(max_length=64)
    nit = models.CharField(max_length=32)

    class Meta:
        verbose_name_plural = "NITs"

    def __str__(self):
        return self.nit

class Direccion(models.Model):
    id_direccion = models.IntegerField(primary_key=True)
    direccion = models.CharField(max_length=100)
    zona = models.IntegerField(default=0)
    colonia = models.CharField(max_length=100)
    departamento = models.CharField(max_length=100)
    ciudad = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = "Direcciones"

    def __str__(self):
        return self.direccion


class Contacto(models.Model):
    id_contacto = models.IntegerField(primary_key=True)
    contacto = models.CharField(max_length=124)
    tipo = models.CharField(max_length=64)

    class Meta:
        verbose_name_plural = "Contactos"

    def __str__(self):
        return self.contacto

class Proveedor(models.Model):
    id_proveedor = models.IntegerField(primary_key=True)
    empresa = models.CharField(max_length=100)
    persona_contacto = models.CharField(max_length=100)
    sitio_web = models.CharField(max_length=512)
    comentarios = models.CharField(max_length=500)
    id_contacto_fk = models.ForeignKey(Contacto, on_delete=models.CASCADE)
    id_direccion_fk = models.ForeignKey(Direccion, on_delete=models.CASCADE)
    id_nit_fk = models.ForeignKey(Nit, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Proveedores"

    def __str__(self):
        return self.id_proveedor

class Producto(models.Model):
    id_producto = models.IntegerField(primary_key=True)
    nombre_producto = models.CharField(max_length=100)
    marca = models.CharField(max_length=100)
    precio_costo = models.DecimalField(max_digits=12, decimal_places=2)
    precio_venta = models.DecimalField(max_digits=12, decimal_places=2)
    imagen = models.CharField(max_length=256)
    descripcion = models.CharField(max_length=500)
    cod_producto = models.CharField(max_length=100)
    id_medida_fk = models.ForeignKey(Medida, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Productos"

    def __str__(self):
        return self.id_producto

class Persona(models.Model):
    id_persona = models.IntegerField(primary_key=True)
    p_nombre = models.CharField(max_length=64)
    s_nombre = models.CharField(max_length=64)
    t_nombre = models.CharField(max_length=64)
    p_apellido = models.CharField(max_length=64)
    s_apellido = models.CharField(max_length=64)
    dpi = models.IntegerField()
    sexo = models.CharField(max_length=1)
    fecha_nacimiento = models.DateField()
    id_nit_fk = models.ForeignKey(Nit, on_delete=models.CASCADE)
    id_direccion_fk = models.ForeignKey(Direccion, on_delete=models.CASCADE)
    id_contacto_fk = models.ForeignKey(Contacto, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Personas"

    def __str__(self):
        return self.id_persona

class Cliente(models.Model):
    id_cliente = models.IntegerField(primary_key=True)
    id_persona_fk = models.ForeignKey(Persona, on_delete=models.CASCADE)
    fecha_creacion = models.DateField(auto_now_add=False)
    numero_compras = models.IntegerField(default=0)

    class Meta:
        verbose_name_plural = "Clientes"

    def __str__(self):
        return self.id_cliente

    def get_absolute_url(self):
        return reverse('company_details')

class Colaborador(models.Model):
    id_colaborador = models.IntegerField(primary_key=True)
    fecha_ingreso = models.DateField()
    fecha_salida = models.DateField()
    salario = models.DecimalField(max_digits=12, decimal_places=2)
    bonificacion = models.DecimalField(max_digits=12, decimal_places=2)
    id_puesto_fk = models.ForeignKey(Puesto_laboral, on_delete=models.CASCADE)
    id_persona_fk = models.ForeignKey(Persona, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Colaboradores"

    def __str__(self):
        return self.id_colaborador

class Detalle_ingreso(models.Model):
    id_detalle_entrada = models.IntegerField(primary_key=True)
    id_producto_fk = models.ForeignKey(Producto, on_delete=models.CASCADE)
    cantidad = models.IntegerField(default=0)
    costo_anterior = models.DecimalField(max_digits=12, decimal_places=2)
    costo_actual = models.DecimalField(max_digits=12, decimal_places=2)
    descuento = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    precio_venta = models.DecimalField(max_digits=12, decimal_places=2)
    sub_total_factura = models.DecimalField(max_digits=12, decimal_places=2)
    total_factura = models.DecimalField(max_digits=12, decimal_places=2)
    fecha_vencimiento = models.DateField()

    class Meta:
        verbose_name_plural = "Detalles de ingresos"

    def __str__(self):
        return self.id_detalle_entrada

class Detalle_salida(models.Model):
    id_detalle_salida = models.IntegerField(primary_key=True)
    id_producto_fk = models.ForeignKey(Producto, on_delete=models.CASCADE)
    cantidad = models.IntegerField(default=0)
    precio_unidad = models.DecimalField(max_digits=12, decimal_places=2)
    descuento = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    total_venta = models.DecimalField(max_digits=12, decimal_places=2)
    tipo_salida = models.CharField(max_length=64)

    class Meta:
        verbose_name_plural = "Detalles de salidas"

    def __str__(self):
        return self.id_detalle_salida

class Ingresos(models.Model):
    id_ingreso = models.IntegerField(primary_key=True)
    fecha_ingreso = models.DateField()
    fecha_factura = models.DateField()
    cod_factura = models.CharField(max_length=100)
    id_proveedor_fk = models.ForeignKey(Proveedor, on_delete=models.CASCADE)
    id_detalle_ingreso_fk = models.ForeignKey(Detalle_ingreso, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Ingresos"

    def __str__(self):
        return self.id_ingreso

class Inventario(models.Model):
    id_inventario = models.IntegerField(primary_key=True)
    id_producto_fk = models.IntegerField()
    existencia = models.IntegerField()

    class Meta:
        verbose_name_plural = "Inventario"

    def __str__(self):
        return self.id_inventario
        
class Resolucion_sat(models.Model):
    id_resolucion = models.IntegerField(primary_key=True)
    nombre_empresa = models.CharField(max_length=100)
    nit_empresa = models.CharField(max_length=100)
    nombre_contribuyente = models.CharField(max_length=100)
    direccion_comercial = models.CharField(max_length=100)
    telefono = models.IntegerField()
    resolucion = models.CharField(max_length=100)
    serie = models.CharField(max_length=100)
    fecha_emision = models.DateField()
    fecha_vencimiento = models.DateField()
    estado = models.CharField(max_length=100)
    numero_factura_inicial = models.IntegerField()
    numero_factura_final = models.IntegerField()
    numero_factura_actual = models.IntegerField()

    class Meta:
        verbose_name_plural = "Resoluciones SAT"

    def __str__(self):
        return self.id_resolucion

class Salida(models.Model):
    id_factura = models.IntegerField(primary_key=True)
    numero_factura = models.CharField(max_length=100)
    id_cliente_fk = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    id_detalle_salida_fk = models.ForeignKey(Detalle_salida, on_delete=models.CASCADE)
    fecha_venta = models.DateField()
    id_colaborador_fk = models.ForeignKey(Colaborador, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Salidas"

    def __str__(self):
        return self.id_factura