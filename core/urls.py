from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.urls import path
from .views import ()
                    
app_name = 'core'
urlpatterns = [
    path('distribuidores/', login_required(lstDistribuidores.as_view()), name='mantenimiento_distribuidores'),
    path('distribuidores/crear/', login_required(crearDistribuidor.as_view()), name='ingresoDistribuidor'),
    path('distribuidores/<pk>/act/', login_required(actualizarDistribuidor.as_view()), name='actualizarDistribuidor'),
]
